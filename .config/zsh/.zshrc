#!/bin/sh

source $HOME/.profile
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# Report status of background jobs immediately
setopt notify

# Completion
autoload -Uz compinit
compinit

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*' '' '' 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' menu select
zstyle :compinstall filename "$XDG_CONFIG_HOME/zsh/zshrc"

zmodload zsh/complist
_comp_options+=(globdots)

# ZSH VIM mode
bindkey -v
export KEYTIMEOUT=1

# History config and search
export HISTFILE="$ZDOTDIR/.histfile"
export HISTSIZE=10000
export SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_SPACE
bindkey '^r' history-incremental-search-backward
bindkey '^n' history-incremental-search-forward

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Misc key bindings
bindkey -s '^k' "$XDG_CONFIG_HOME/"
bindkey -v '^?' backward-delete-char
bindkey '^[[P' delete-char

# Enable colors
autoload -U colors && colors

# Init the prompt
export STARSHIP_CONFIG="$ZDOTDIR/starship.toml"
eval "$(starship init zsh)"

# Load syntax highlighting; should be last.
source "$ZDOTDIR/plugins/fsh/fast-syntax-highlighting.plugin.zsh" 2>/dev/null
