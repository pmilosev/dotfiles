on run ARGV
    # DUE TO MACOS SECURE KEYBOARD I CAN NOT TRIGGER PASSWORD MANAGEMENT ON PASSWORD FILEDS
    # THEREFORE IT IS ALWAYS TRIGGERED FROM ANOTHER APP (E.G. FINDER) AND THIS SCRIPT SWITCHES
    # TO THE APP REQUIRING THE PASSWORD USING CMD - TAB

    do shell script "skhd -k 'cmd - tab'"

    # THIS SCRIPT WILL LAUNCH A TERMINAL WINDOW
    # IF THE PASSWORD FILED IS WITHIN ANOTHER TERMINAL WINDOW USE CMD - ~
    # OTHERWISE USE CMD - TAB TO SWITCH BACK TO IT

    set SWITCHER to "skhd -k 'cmd - tab'"
    tell application "System Events"
        tell application process "Terminal"
            if get frontmost then
                set SWITCHER to "skhd -k 'cmd - 0x32'"
            end if
        end tell
    end tell

    # LAUNCH VIFM IN A NEW TERMINAL WINDOW AND USE THE ON-CHOOSE OPTION TO RUN THE PASSWORD
    # MANAGER ON THE CHOOSEN PASSWORD FILE
    # IF ARGV IS NOT EMPTY IT IS USED AS KEY FOR A NON-PASSWORD RECORD TO BE LOADED (e.g. LOGIN)

    tell application "Terminal"
        activate

        set ONCHOOSE to SWITCHER
        if length of ARGV > 0 then
            set ONCHOOSE to ONCHOOSE & " && type-pass --record " & ARGV & " '%d/%c'"
        else
            set ONCHOOSE to ONCHOOSE & " && type-pass '%d/%c'"
        end if

        do script "vifm --on-choose \"" & ONCHOOSE & "\" $PASSWORD_STORE_DIR/ && exit"
    end tell
end run
