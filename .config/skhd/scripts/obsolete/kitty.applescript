set TERMINALRUNNING to false
tell application "System Events"
    set TERMINALRUNNING to (name of processes) contains "kitty"
end tell

tell application "kitty"
    activate
    if TERMINALRUNNING then
        do shell script "skhd -k 'cmd - n'"
    end if
end tell
