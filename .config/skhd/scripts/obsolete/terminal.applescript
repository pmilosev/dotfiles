set TERMINALRUNNING to false
tell application "System Events"
    set TERMINALRUNNING to (name of processes) contains "Terminal"
end tell

tell application "Terminal"
    activate
    if TERMINALRUNNING then
        do script ""
    end if
end tell
