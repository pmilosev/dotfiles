on run ARGV
    # SOMETIMES IT'S USEFUL TO JUST USE VIFM TO GET A FILE PATH
    # AN EXTRA ARGUMENT SEND TO THE SCRIPT INSTRUCTS VIFM NOT TO
    # OPEN THE FILE BUT RATHER TO RETURN IT'S PATH WHICH IS THEN
    # COPIED TO CLIPBOARD
    set CMD to "vifm"
    if length of ARGV > 0 then
        set CMD to "(vifm --choose-files - --choose-dir - | head -1 | tr -d '\n' | pbcopy)"
    end if

    set TERMINALINFOCUS to false
    tell application "System Events"
        tell application process "Terminal"
            set TERMINALINFOCUS to get frontmost
        end tell
    end tell

    tell application "Terminal"
        if not TERMINALINFOCUS then
            activate
            set CMD to CMD & " && skhd -k 'cmd - tab'"
        end if

        do script CMD & " && exit"
    end tell
end run
