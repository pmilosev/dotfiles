#!/bin/sh

. $HOME/.profile
export BDOTDIR="$XDG_CONFIG_HOME/bash"

# Bash history
# Ignore duplicates or lines starting with a space
export HISTFILE="$BDOTDIR/.histfile"
export HISTSIZE=3000
export HISTFILESIZE=3000
export HISTCONTROL=ignoreboth

# Init the prompt
export STARSHIP_CONFIG="$BDOTDIR/starship.toml"
eval "$(starship init bash)"
