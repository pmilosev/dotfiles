# Shared BASH and ZSH environment

# Language
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# CLI / *nix software management
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.local/cache"
export USR_INSTALL="$HOME/.local/install"
export USR_BIN="$HOME/.local/bin"

# HOMEBREW over SYSTEM; USER over HOMEBREW
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
export PATH="$USR_BIN:$USR_INSTALL/homebrew/bin:$USR_INSTALL/homebrew/sbin:$PATH"

# HOMEBREW Cask into local Applications
export HOMEBREW_CASK_OPTS="--appdir=$HOME/Applications"

# Tell misc apps where to find their config
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export HIGHLIGHT_DATADIR="$XDG_CONFIG_HOME/highlight/"

export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export LESSHISTFILE="$XDG_DATA_HOME/lesshst"

# Android environment
export ANDROID_ROOT="$HOME/.android"
export ANDROID_SDK_ROOT="$ANDROID_ROOT/sdk"
export ANDROID_SDK_HOME="$ANDROID_ROOT/sdk"
export ANDROID_EMULATOR_HOME="$ANDROID_ROOT"
export ANDROID_AVD_HOME="$ANDROID_ROOT/avd"
export ANDROID_NDK_HOME="$ANDROID_ROOT/ndk"
export ANDROID_PATH="$ANDROID_SDK_HOME/tools:$ANDROID_SDK_HOME/tools/bin:$ANDROID_SDK_HOME/platform-tools:$ANDROID_SDK_HOME/platform-tools/bin::$ANDROID_SDK_HOME/build-tools:$ANDROID_SDK_HOME/build-tools/bin:$ANDROID_NDK_HOME"
export PATH="$PATH:$ANDROID_PATH"

# GPG agent setup
export GPG_TTY=$(tty)

# Colours
export LESS=-R
alias \
    diff="colordiff"         \
    grep="grep --color=auto" \
    hl="highlight --force=sh --out-format=ansi"     \
    ls="gls --color=auto --group-directories-first" \
    ll="ls -lAh"

function vdiff {
    diff -yd $@ | less -R
}

# Interactive
alias \
    cp="cp -iv" \
    mv="mv -iv" \
    rm="rm -iv" \
    mkdir="mkdir -pv"

function rm {
    echo nope!
}
function rmt {
    mv $@ "$HOME/.Trash/"
}

# NVIM
export EDITOR="nvim"
export VISUAL="nvim"
alias \
    vi="nvim"         \
    vim="nvim"        \
    vimdiff="nvim -d"

function magit {
(
    [ $# -gt 0 ] && cd "$1"
    nvim -c MagitOnly
)
}

# Notebook
alias note='cd ~/Тефтер/ && nvim ./index.md && cd -'

# MacOS
alias os="osascript"

# NOHUP in /tmp
alias nohup='cd /tmp && nohup '

# OpenAI-CLI load token from PASS
function gpt {
    if [ $# -eq 0 ]; then
        openai repl -t $(pass personal/provided/openai-cli-api-key)
    else
        echo "$@" | openai complete -t $(pass personal/provided/openai-cli-api-key) -
    fi
}

# Dotfiles management
alias dotfiles="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
