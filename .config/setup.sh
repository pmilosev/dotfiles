#!/bin/sh

. $HOME/.profile
SCRIPT="$0"
MODULE="$1"; shift
PARAMS="$@"

usage () {
	cat << EOM
Usage:
	$ $SCRIPT [module] [module arguments]

Modules:
	$(grep '^[a-zA-Z0-9_]* ()' $SCRIPT | cut -d' ' -f1 | xargs)

EOM
}

dotfiles_setup () {
	cat << EOM
============================================================
Setting up XDG & Dotfiles
============================================================
EOM
	[ ! -d "$HOME/.config" ] && mkdir "$HOME/.config"
	[ ! -d "$HOME/.local" ] && mkdir "$HOME/.local"
	[ ! -d "$HOME/.local/install" ] && mkdir "$HOME/.local/install"
	[ ! -d "$HOME/.local/share" ] && mkdir "$HOME/.local/share"
	[ ! -d "$HOME/.local/bin" ] && mkdir "$HOME/.local/bin"

	git clone --separate-git-dir=$HOME/.dotfiles https://gitlab.com/pmilosev/dotfiles.git /tmp/dotfiles && \
	rsync --links --recursive --verbose --exclude '.git' /tmp/dotfiles/ $HOME/ && \
	rm -r /tmp/dotfiles && \
	. $HOME/.profile

}

homebrew_setup () {
	cat << EOM
============================================================
Setting HOMEBREW
============================================================
EOM

	[ ! -d "$HOME/.local/install/homebrew" ] && mkdir "$HOME/.local/install/homebrew"
	curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C $HOME/.local/install/homebrew && \
	brew update
}

homebrew_packages () {
	cat << EOM
============================================================
Installing packages (HOMEBREW)
============================================================
EOM

	brew install $(grep -v '^#' $HOME/.config/homebrew/leaves | xargs) && \
	brew cask install $(grep -v '^#' $HOME/.config/homebrew/casks | xargs)
}

install_fonts () {
	cat << EOM
============================================================
Installing fonts
============================================================
EOM

    find $HOME/.config/fonts -type f -exec cp {} $HOME/Library/Fonts \;
}

apply_preferences () {
	cat << EOM
============================================================
Applying MacOS & Applications preferences
============================================================
EOM

    $HOME/.config/macOS/.macos

}

skhd_service () {
	cat << EOM
============================================================
Starting hot key daemon
============================================================
EOM
    brew services start skhd
}

main () {
	cat << EOM
============================================================
Complete setup
============================================================

EOM
	dotfiles_setup && \
	homebrew_setup && \
	homebrew_packages && \
    install_fonts && \
    apply_preferences && \
    skhd_service

    echo "DONE! DONE! DONE!"
    echo "You probably want to restart your computer now !"
}

[ -n "$MODULE" ] || MODULE='main'
[ '-h' = "$MODULE" ] && MODULE='usage'
$MODULE $PARAMS
