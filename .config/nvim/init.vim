"{{{ PLUGINS
"{{{2 Setup Plug
if ! filereadable(expand("$HOME/.config/nvim/autoload/plug.vim"))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p $HOME/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > $HOME/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif
"}}}2
"{{{2 Plugins
call plug#begin(expand("$HOME/.config/nvim/plugged"))
Plug 'tpope/vim-sensible'               " Further sensible defaults
Plug 'tpope/vim-speeddating'            " Manage dates easily
Plug 'tpope/vim-commentary'             " Manage comments easily
Plug 'tpope/vim-afterimage'             " Edit text equivalent of bin files (eg. plist)

Plug 'tpope/vim-fugitive'               " Premier Git plugin for Vim
Plug 'junegunn/gv.vim'                  " GIT commit browser
Plug 'jremmen/vim-ripgrep'              " RipGrep for VIM
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

Plug 'gerazov/vim-macedonian'
Plug 'kshenoy/vim-signature'

Plug 'NLKNguyen/papercolor-theme'       " Colorscheme
Plug 'scrooloose/nerdtree'              " Manage files with NerdTree
Plug 'vim-airline/vim-airline'          " Minimal status bar
Plug 'vim-airline/vim-airline-themes'

Plug 'jreybert/vimagit'                 " Magit for VIM
Plug 'keith/swift.vim'                  " Swift syntax support
Plug 'dr-kino/cscope-maps'              " CScope support

Plug 'vimwiki/vimwiki'                  " Take notes with VimWiki
Plug 'mattn/calendar-vim'               " Calendar (integrates with vimwiki)
Plug 'junegunn/limelight.vim'           " Focussed writing
Plug 'adelarsq/vim-pomodoro'            " Pomodoro for vim
Plug 'michal-h21/vim-zettel'            " Vimwiki compatible Zettelkasten

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'dhruvasagar/vim-table-mode'       " Support for formatting tables
Plug 'godlygeek/tabular'                " Helps to vertical align content
Plug 'wellle/targets.vim'               " Extra text objects

Plug 'jamessan/vim-gnupg'               " Edit encrypted files

Plug 'dense-analysis/ale'               " Asynchronous lint engine
Plug '0xStabby/chatgpt-vim'             " OpenAI (ChatGPT) integration
" Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
call plug#end()
"}}}2
"{{{2 Plugin config

" Colorscheme
source $HOME/.config/nvim/theme/theme.vim
let g:PaperColor_Theme_Options = {
    \   'theme': {
    \     'default': {
    \       'transparent_background': 0,
    \       'allow_bold': 1,
    \       'allow_italic': 1
    \     },
    \     'default.dark': {
    \       'override' : {
    \         'color05' : ['#a8a8a8', '']
    \       }
    \     },
    \     'default.light': {
    \       'override' : {
    \         'color00' : ['#ffffff', ''],
    \         'color07' : ['#000000', '']
    \       }
    \     }
    \   }
    \ }

colorscheme PaperColor

" NERDTree
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

" VimWiki
let repo_notebook = {}
let repo_notebook.path = '~/Бележник/'
let repo_notebook.path_html = '~/Бележник/.html/'
let repo_notebook.template_path = '~/Бележник/.conf/'
let repo_notebook.diary_rel_path = 'Дневник/'
let repo_notebook.diary_index = 'index'
let repo_notebook.diary_header = 'Хроника'
let repo_notebook.auto_diary_index = 1
let repo_notebook.links_space_char = '-'
let repo_notebook.syntax = 'markdown'
let repo_notebook.ext = 'md'

let g:vimwiki_list = [repo_notebook]
let g:vimwiki_folding = 'custom'
let g:vimwiki_global_ext = 0
let g:vimwiki_markdown_link_ext = 1
let g:vimwiki_url_maxsave = 0
let g:vimwiki_list_ignore_newline = 0
let g:vimwiki_auto_chdir = 1

let g:vimwiki_diary_months = {
      \ 1: 'Коложег (јануари)', 2: 'Сечко (февруари)', 3: 'Цутар (март)',
      \ 4: 'Тревен (април)', 5: 'Косар (мај)', 6: 'Жетвар (јуни)',
      \ 7: 'Златец (јули)', 8: 'Житар (август)', 9: 'Гроздобер (септември)',
      \ 10: 'Листопад (октомври)', 11: 'Студен (ноември)', 12: 'Снежник (декември)'
      \ }


" restore markdown highlighting even when vimwiki takes over
autocmd bufenter *.md set syntax=markdown

" generate template body for new diary entries
au BufNewFile ~/Бележник/Дневник/*.md :silent 0r !~/Бележник/.conf/diary-tpl ~/Бележник/Дневник/
au BufWritePost ~/Бележник/Дневник/*.md :silent !~/Бележник/.conf/diary-upd ~/Бележник/Дневник/

" Zettelkasten config
let zettel_notebook = {}
let zettel_notebook.template = "~/Бележник/.conf/zettel.tpl"
let zettel_notebook.disable_front_matter = 1

let g:zettel_options = [zettel_notebook]
let g:zettel_fzf_command = "rg --column --line-number --ignore-case --no-heading --color=always "
let g:zettel_generated_index_title = "Zettelkasten Index"

" Calendar
" H,L navigate dates; Enter yanks the date
au FileType calendar nmap <buffer> l /\( \\|\d\)\d\(+\\|*\\| \)<CR>:noh<CR>
au FileType calendar nmap <buffer> h lNN:noh<CR>
function CalendarYank(day,month,year,week,dir)
    let l:month = len(a:month) == 1 ? '0' . a:month : a:month
    let l:week = len(a:week) == 1 ? '0' . a:week : a:week
    let l:day = len(a:day) == 1 ? '0' . a:day : a:day

    let @" = a:year . '-' . l:month . '-' . l:day . ' KW' . l:week
    let @+ = a:year . '-' . l:month . '-' . l:day . ' KW' . l:week
    quit
endfunction
let calendar_action = 'CalendarYank'
let g:calendar_no_mappings=0
let g:calendar_monday=1
let g:calendar_weeknm=3
let g:calendar_datetime = 'statusline'

" Pomodoro config
let g:pomodoro_time_work = 25
let g:pomodoro_time_slack = 5
let g:pomodoro_do_log = 1
let g:pomodoro_log_file = repo_notebook.path . '/.pomodoro.log'
let g:pomodoro_notification_cmd = "osascript -e " . "'display notification " . '"POMODORO FINISHED" with title "pomodoro-vim"' . "'"

" Markdown
let g:mkdp_auto_start = 0
let g:mkdp_auto_close = 0
let g:mkdp_refresh_slow = 1
let g:mkdp_command_for_global = 1

" Table mode
let g:table_mode_corner='|'
let g:table_mode_map_prefix='<Leader>b'
let g:table_mode_tableize_d_map='<Leader>B'

let g:table_mode_motion_up_map='<Leader>bk'
let g:table_mode_motion_down_map='<Leader>bj'
let g:table_mode_motion_left_map='<Leader>bh'
let g:table_mode_motion_right_map='<Leader>bl'

let g:table_mode_tableize_map=g:table_mode_map_prefix.'b'
let g:table_mode_update_time=200

" ALE
let g:ale_fixers = ['prettier']
let g:ale_lint_on_enter = 0

" OpenAI (ChatGPT) config
let g:openaiToken = system("pass personal/provided/openai-cli-api-key")

" Firevim
" let g:firenvim_config = {
"     \   'globalSettings': {
"     \       'alt': 'all'
"     \   },
"     \   'localSettings': {
"     \       '.*': {
"     \           'cmdline': 'neovim',
"     \           'content': 'text',
"     \           'selector': 'textarea:not([readonly]), div[role="textbox"]',
"     \           'takeover': 'never',
"     \           'priority': 0
"     \       },
"     \       'plaza\..*\.com': {
"     \           'cmdline': 'neovim',
"     \           'content': 'html',
"     \           'selector': '#tinymce',
"     \           'takeover': 'never',
"     \           'priority': 1
"     \       },
"     \       '.*/jira': {
"     \           'cmdline': 'neovim',
"     \           'content': 'html',
"     \           'selector': '#tinymce',
"     \           'takeover': 'never',
"     \           'priority': 1
"     \       }
"     \   }
"     \ }
"}}}2
"}}}

"{{{ GENERAL SETTINGS
" A lot more managed by vim-sensible
set nocompatible                        " Don't try to be VI
set number relativenumber               " Relative line numbers
set path+=**                            " Search file recursively
set ignorecase                          " Case insensitive search
set smartcase                           " Smart case matching

set foldlevel=0                         " Start with folds closed
set foldminlines=0                      " Single lines can be folded
set foldnestmax=4                       " Up to four nested folds
set foldcolumn=auto:4                   " Side column accomodating four fold levels
set foldmethod=marker                   " Folds defined by markers (by default)

set expandtab                           " Use spaces instead of tabs
set shiftwidth=4                        " One tab == four spaces
set tabstop=4                           " One tab == four spaces

set colorcolumn=100                     " Visual right margin (half MacBook screen)
set cursorline                          " Highlight current line
set nowrap                              " Don't wrap lines

set guifont=Hack:h16,monospace:h16      " GUI font - e.g. for firenvim
set splitbelow splitright               " Split new windows below and right

set conceallevel=0                      " Show text as is, no hiding during formatting

" Useful settings for project work
command! Ctags !ctags -R .              " Build ctags
autocmd BufWritePre * %s/\s\+$//e       " Delete trailing whitespaces on save
autocmd BufWritePre * %s/\n\+\%$//e     " Delete trailing newlines on save

" Persistent undo
if !isdirectory("/tmp/vim-undo-dir")
    call mkdir("/tmp/vim-undo-dir", "", 0700)
endif
set undodir=/tmp/vim-undo-dir
set undofile

" Restore position after reopening a file
autocmd BufReadPost *
   \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
   \ |   exe "normal! g`\""
   \ | endif

" Auto open/close file managers
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" I actually like auto comments
autocmd FileType * setlocal formatoptions+=c formatoptions+=r formatoptions+=o

" Support for Macedonian spelling and typing
set spelllang=en,mk
function LazyLoadKeymap()
    if len(&keymap) > 0
        set keymap=
    else
        set keymap=macedonian
    endif
endfunction

" Vimwiki additions
" Toggle TODOs markup
function VimwikiToggleCB()
    let l:line = getline('.')
    if l:line =~# '^[ 	]*- \[[ .oO-X]\] '
        :VimwikiRemoveSingleCB
    else
        :s/^\([ 	]*\)-* */\1- [ ] /
    endif
endfunction

" Move the cursor to the tagging position
" filling in spaces as needed
function VimwikiTagsPosition()
    execute "normal! $"
    let l:num_spaces = 100 - getcursorcharpos()[2]
    if l:num_spaces > 0
        execute "normal! " . l:num_spaces . "A "
    endif

    execute "normal! ^99l"
endfunction

" Helpper functions used to discover relevant due dates and open tasks
" Duedate format:
"   :<:yyyy-mm-dd:   task to be done until the tagged date
"   :=:yyyy-mm-dd:   task to be done at the tagged date
"   :>:yyyy-mm-dd:   task to be done after the tagged date
" Dates can miss a component (e.g. yyyy-mm is accepted) but
" not a digit (e.g. yy-m-d not accepted)
" A duedate tag can be followed by a repetition tag that defines
" the rate (cycle) in days at which the task is repeated.
" E.g.:
"   :<:2023-12-21:@3
" will repeat (pop-up in discovery) every 3 days until 2023-12-21
" with the first appearance be on the first date whose distance from the given
" date is divisible by 3.
" While:
"   :>:2023-05-01:@5:<2024:
" will pop-up every fifth day starting 2023-05-01, until 2024-01-01.
let s:vim_wiki_duedate_cmd = "" .
    \ "rg --vimgrep --no-column ':[<>=]:[0-9]{4}(-[0-9]{2}){0,2}(:@[0-9]+:){0,1}' | " .
    \ "awk -F: " .
    \ "-v cur_date=$(date +%Y%m%d) " .
    \ "-v cur_seconds=$(date +%s) " .
    \ " '{ " .
    \ '     matched = 0; conditions = 0; sort_date = -99999999; ' .
    \ '     for (i=1; i<NF; i++) { ' .
    \ '         relation = $i; rel_date = $(i+1); cycle=$(i+2); offset = 5;' .
    \ '         if ((relation ~ /^[<>=]$/) && (rel_date ~ /^[0-9]{4}(-[0-9]{2}){0,2}$/)) { ' .
    \ '             if (relation == "=") { ' .
    \ '                 if (rel_date ~ /^[0-9]{4}$/) { sub(/$/, "-04", rel_date) } ' .
    \ '                 if (rel_date ~ /^[0-9]{4}-[0-9]{2}$/) { sub(/$/, "-10", rel_date) } ' .
    \ '                 offset = 0; ' .
    \ '                 relation = "<"; ' .
    \ '             } ' .
    \ '             if (relation == "<") { ' .
    \ '                 if (rel_date ~ /^[0-9]{4}$/) { sub(/$/, "-01", rel_date) } ' .
    \ '                 if (rel_date ~ /^[0-9]{4}-[0-9]{2}$/) { sub(/$/, "-01", rel_date) } ' .
    \ '                 gsub(/-/, "", rel_date); ' .
    \ '                 conditions = conditions + 1; ' .
    \ '                 if (cycle ~ /^@[0-9]+$/) { ' .
    \ '                     matched = matched + 1; ' .
    \ '                 } else { ' .
    \ '                     matched = matched + (rel_date - cur_date <= offset); ' .
    \ '                 } ' .
    \ '             } ' .
    \ '             if (relation == ">") { ' .
    \ '                 if (rel_date ~ /^[0-9]{4}$/) { sub(/$/, "-12", rel_date) } ' .
    \ '                 if (rel_date ~ /^[0-9]{4}-[0-9]{2}$/) { sub(/$/, "-31", rel_date) } ' .
    \ '                 gsub(/-/, "", rel_date); ' .
    \ '                 conditions = conditions + 1; ' .
    \ '                 matched = matched + (cur_date >= rel_date); ' .
    \ '             } ' .
    \ '             if (cycle ~ /^@[0-9]+$/) { ' .
    \ '                 sub(/@/, "", cycle); ' .
    \ '                 dts_cmd = "date -j -f %Y%m%d " rel_date " +%s"; ' .
    \ '                 dts_cmd | getline rel_seconds; close(dts_cmd); ' .
    \ '                 rel_days = int(rel_seconds / 86400); ' .
    \ '                 cur_days = int(cur_seconds / 86400); ' .
    \ '                 dif_days = cur_days - rel_days; ' .
    \ '                 dif_days = dif_days < 0 ? 0 - dif_days : dif_days; ' .
    \ '                 conditions = conditions + 1; ' .
    \ '                 matched = matched + ((dif_days % cycle) <= offset); ' .
    \ '             } ' .
    \ '             sort_date = sort_date < rel_date ? rel_date : sort_date; ' .
    \ '         } ' .
    \ '     } ' .
    \ '     if (matched && (matched == conditions)) { print sort_date ":" $0 }' .
    \ " }' | " .
    \ " sort -t: -k2,3 -u | " .
    \ " sort -t: -k1 | " .
    \ " cut -d: -f2- "

" Command for discovery of open tasks without a date tag
let s:vim_wiki_unscheduled_cmd = "" .
    \ "rg --vimgrep --no-column '^[ 	]*- \\[ \\]' | " .
    \ "rg -v '^.*:[0-9]*:.*:[<>=]:[0-9]{4}(-[0-9]{2}){0,2}'"

" List discovered duedates in location list
function VimwikiDueDates()
    :lgetexpr system(s:vim_wiki_duedate_cmd)
    :laddexpr system(s:vim_wiki_unscheduled_cmd)
    :lopen
endfunction

" Insert discovered duedates below cursor position
function VimwikiGenerateDueDatesLinks()
    let l:cmd = s:vim_wiki_unscheduled_cmd . " | " .
                \ "awk -F: '{ " .
                \ '  print "- [line:" $2 "](/" $1 "):: " $0 ' .
                \ " }' | " .
                \ " sed 's/:: [^:]*:[0-9]*:/:: /' "

    let @" = system(l:cmd)
    :normal ""p
    let l:cmd = s:vim_wiki_duedate_cmd . " | " .
                \ "awk -F: '{ " .
                \ '  print "- [line:" $2 "](/" $1 "):: " $0 ' .
                \ " }' | " .
                \ " sed 's/:: [^:]*:[0-9]*:/:: /' "

    let @" = system(l:cmd)
    :normal ""P
endfunction

" Adding support for outlook: scheme in vimwiki links
" Adding support for shortened links
function! VimwikiLinkHandler(link)
    let l:email_id = ""
    if (a:link =~ "outlook:")
        let [l:scheme, l:email_id] = split(a:link, ':')
        let l:url = "https://outlook.office365.com/mail/deeplink?ItemID=" . l:email_id
        :call vimwiki#base#system_open_link(l:url)
        return 1
    endif

    if (a:link =~ "shrt:")
        let l:dbfile = '.shrt'
        if (!filereadable(l:dbfile)) " fail if the db file does not exist
            return 0
        endif

        let l:cmd = "grep '^" . a:link . " --> ' " . l:dbfile . " | tr -d '\\n'"
        let l:dbline = system(l:cmd)
        if (v:shell_error != 0)
            return 0
        endif

        let [l:shrt, l:link] = split(l:dbline, ' --> ')
        if (VimwikiLinkHandler(l:link) == 0)
            :call vimwiki#base#open_link(':edit', l:link)
        endif
        return 1
    endif

    return 0
endfunction

" Vimwiki link shortener
function VimwikiLinkShortenerToggle()
    " assuming PWD is Vimwiki root, which is expected behaviour
    " unless manually CD into something, which is acceptable risk
    let l:dbfile = '.shrt'
    if (!filereadable(l:dbfile)) " fail if the db file does not exist
        return 0
    endif

    " assuming properly positioned, and current or next () is
    " where the link is defined
    :normal di(
    let l:link = @"
    if (len(l:link) < 37) " don't shorten if already shorter than the result
        let @" = l:link
        :normal ""P
        return 0
    endif

    " if short, than restore from db
    if (l:link =~ "shrt:")
        let l:cmd = "grep '^" . l:link . " --> ' " . l:dbfile . " | tr -d '\\n'"
        let l:dbline = system(l:cmd)
        if (v:shell_error != 0) " if not found put back the shortened form
            let @" = l:link
            :normal ""P
            return 0
        endif

        let [l:tmp, l:link] = split(l:dbline, ' --> ')
        let @" = l:link
        :normal ""P
        return 1
    endif

    " md5 should be good enough for hashing
    let l:cmd = "echo '" . l:link . "' | md5sum | cut -d' ' -f1 | tr -d '\\n'"
    let l:shrt = 'shrt:' . system(l:cmd)
    if ((v:shell_error != 0) || (len(l:shrt) != 37)) " restore if any error
        let @" = l:link
        :normal ""P
        return 0
    endif

    " test if the link is already in the db
    let l:dbline = l:shrt . ' --> ' . l:link
    let l:cmd = "grep '^" . l:dbline . "$' " . l:dbfile
    let l:tmp = system(l:cmd)
    if (v:shell_error == 0) " finish with success if found
        let @" = l:shrt
        :normal ""P
        return 1
    elseif (v:shell_error == 1) " if search successful but not found
        let l:cmd = "echo '" . l:dbline . "' >> " .l:dbfile
        let l:tmp = system(l:cmd)
    endif

    " searching or inserting into the db failed
    " restore the link and fail
    if (v:shell_error != 0)
        let @" = l:link
        :normal ""P
        return 0
    endif

    let @" = l:shrt
    :normal ""P
    return 1
endfunction
"}}}

"{{{ MAPPINGS
" Yank to end of line
map Y y$

let mapleader = " "
nnoremap <leader>k  :e $HOME/.config/nvim/init.vim<CR>
nnoremap <leader>r  :source $HOME/.config/nvim/init.vim<CR>
nnoremap <leader>t  :new term://bash<CR>
nnoremap <leader>s  :setlocal spell!<CR>
nnoremap <leader>// :set hlsearch<CR>
nnoremap <leader>/c :set nohlsearch<CR>

" More intuitive splitting
nnoremap <leader>vv :vsplit<CR>
nnoremap <leader>vc :close<CR>
nnoremap <leader>v+ :vertical resize +9<CR>
nnoremap <leader>v- :vertical resize -9<CR>
nnoremap <leader>v= <C-W>=
nnoremap <leader>vh <C-w>t<C-w>K
nnoremap <leader>hh :split<CR>
nnoremap <leader>hc :close<CR>
nnoremap <leader>h+ :resize +9<CR>
nnoremap <leader>h- :resize -9<CR>
nnoremap <leader>h= <C-W>=
nnoremap <leader>hv <C-w>t<C-w>H

" Easier split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" NERDTree
nnoremap <leader>nn :NERDTreeToggle<CR>
nnoremap <leader>nf :NERDTreeFind<CR>
nnoremap <leader>nc :NERDTreeClose<CR>

" Markdown
nnoremap <leader>mm :MarkdownPreview<CR>
nnoremap <leader>mc :MarkdownPreviewStop<CR>
nnoremap <leader>mh :% !pandoc -f markdown -t html - -o - \| prettier --parser html
nnoremap <leader>mH :% !pandoc -f html -t markdown - -o - \| prettier --parser markdown
vnoremap <leader>mh :!pandoc -f markdown -t html - -o - \| prettier --parser html
vnoremap <leader>mH :!pandoc -f html -t markdown - -o - \| prettier --parser markdown

" Vimwiki
autocmd FileType vimwiki nmap gl<enter> :call VimwikiLinkShortenerToggle()<CR>
autocmd FileType vimwiki nmap gl<space> :call VimwikiToggleCB()<CR>
autocmd FileType vimwiki nmap glv       <C-Space>

autocmd FileType vimwiki nmap gtd       :call VimwikiDueDates()<CR>
autocmd FileType vimwiki nmap gtl       :call VimwikiGenerateDueDatesLinks()<CR>
autocmd FileType vimwiki nmap gto       :Rg -F ' [ ] '<CR>
autocmd FileType vimwiki nmap gtt       :call VimwikiTagsPosition()<CR>
autocmd FileType vimwiki nmap gta       :execute "normal A<:" . strftime('%Y-%m-%d') . ":"<CR>
autocmd FileType vimwiki nmap gti       :execute "normal i:<:" . strftime('%Y-%m-%d')<CR>
autocmd FileType vimwiki nmap gtx       di:x

" Zettelkasten
autocmd FileType vimwiki nmap zzz :ZettelNew<CR>
autocmd FileType vimwiki nmap zzi :ZettelInsertNote<CR>
autocmd FileType vimwiki nmap zzo :ZettelOpen<CR>

" Macedonian spelling and typing
inoremap <C-^> <C-o>:call LazyLoadKeymap()<CR>

" Tabularize
noremap <leader>ll :Tabularize /

" Calendar
nnoremap <leader>cc :CalendarH<CR>

"Pomodoro
nnoremap <leader>pp :PomodoroStart
nnoremap <leader>ps :PomodoroStatus<CR>
nnoremap <leader>pc :PomodoroCancel

" ALE
nnoremap <leader>fa :<Plug>(ale_fix)<CR>
nnoremap <leader>ff :ALEToggleBuffer<CR>
nnoremap <leader>fc :ALEDisableBuffer<CR>
