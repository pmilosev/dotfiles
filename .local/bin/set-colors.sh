#!/bin/sh

theme="$1"

if [ "$theme" = "dark" ]; then
    ln -fs $XDG_CONFIG_HOME/kitty/papercolor-kitty/papercolor-dark.conf $XDG_CONFIG_HOME/kitty/theme.conf
    ln -fs $XDG_CONFIG_HOME/nvim/theme/dark.vim $XDG_CONFIG_HOME/nvim/theme/theme.vim
    ln -fs $XDG_CONFIG_HOME/vifm/colors/papercolor-dark.vifm $XDG_CONFIG_HOME/vifm/colors/theme.vifm
    exit 0;
fi

if [ "$theme" = "light" ]; then
    ln -fs $XDG_CONFIG_HOME/kitty/papercolor-kitty/papercolor-light.conf $XDG_CONFIG_HOME/kitty/theme.conf
    ln -fs $XDG_CONFIG_HOME/nvim/theme/light.vim $XDG_CONFIG_HOME/nvim/theme/theme.vim
    ln -fs $XDG_CONFIG_HOME/vifm/colors/papercolor-light.vifm $XDG_CONFIG_HOME/vifm/colors/theme.vifm
    exit 0;
fi

cat << EOM
Sets compatible colorschemes for:
    - Kitty
    - NeoVim
    - Vifm

Usage:
    \$$0 <dark | light>
EOM

exit 1
